<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ログイン</title>
    <!-- BootstrapのCSS読み込み -->
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- オリジナルCSS読み込み -->
    <link href="css/original/common.css" rel="stylesheet">
</head>
<body>
<form action="LoginServlet" method="post">
<nav class="navbar navbar-expand-lg navbar-light" style="background-color:#EDFFBE;">
  <a class="navbar-brand" href="LoginServlet">サインイン
  </a>
</nav>
<div class="login-area">
	<p><img src="img/login.png" alt="tap">
	</p>
<div class="form-group has-warning has-feedback">
	  <label class="control-label" for="inputWarning2">LoGin
	  </label>
	  <input type="text" class="form-control" name="loginId" id="inputWarning2" aria-describedby="inputWarning2Status" placeholder="abcde123">
</div>
 <div class="form-group has-error has-feedback">
  <label class="control-label" for="inputError2">PasSword
  </label>
  <input type="password" class="form-control" name="password" id="inputError2" aria-describedby="inputError2Status" placeholder="Password">
 </div>
 <span >
 <font color="red">${errMsg}
 </font>
 </span><br>
	<button type="submit" class="btn btn-warning btn-sm">ログイン
	</button>
 </div>
</form>
</body>
</html>
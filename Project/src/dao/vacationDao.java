package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.vacationBeans;;

public class vacationDao {

	 public vacationBeans vacationJoin(String userId) {
			Connection conn = null;

			try {
				// データベース接続
				conn = DBmanager.getConnection();
				// 実行SQL文字列定義
				String sql = "SELECT * FROM vacation WHERE user_id = ?";

				PreparedStatement pStmt = conn.prepareStatement(sql);
				// SQLの?パラメータに値を設定
				pStmt.setString(1,userId);

				ResultSet rs = pStmt.executeQuery();
				 if (!rs.next()) {
		                return null;
		            }

				    int Id = rs.getInt("id");
					String vacationF = rs.getString("vacation_dateFirst");
					String vacationS = rs.getString("vacation_dateSecond");
					String vacationT = rs.getString("vacation_dateThird");
					String vacationFo = rs.getString("vacation_dateFour");
		            return new vacationBeans(Id,userId,vacationF,vacationS,vacationT,vacationFo);
		        } catch (SQLException e) {
		            e.printStackTrace();
		            return null;
		        } finally {
		            // データベース切断
		            // 以下findAllと同じ処理なので略
		        }
		    }

	 public List<vacationBeans> findAll() {
	        Connection conn = null;
	        List<vacationBeans> vacationList = new ArrayList<vacationBeans>();

	        try {
	            // データベースへ接続
	            conn = DBmanager.getConnection();

	            // SELECT文を準備
	            String sql = " SELECT vacation.id, vacation.user_id, vacation.vacation_dateFirst, vacation.vacation_dateSecond, vacation.vacation_dateThird, vacation.vacation_dateFour, user.name FROM vacation INNER JOIN user user ON vacation.user_id = user.id where user.name <> '河野菜摘'";

	             // SELECTを実行し、結果表を取得
	            Statement stmt = conn.createStatement();
	            ResultSet rs = stmt.executeQuery(sql);

	            // 結果表に格納されたレコードの内容を
	            // Userインスタンスに設定し、ArrayListインスタンスに追加
	            while (rs.next()) {
	                int Id = rs.getInt("id");
					String userIdDate = rs.getString("user_id");
					String vacationF = rs.getString("vacation_dateFirst");
					String vacationS = rs.getString("vacation_dateSecond");
					String vacationT = rs.getString("vacation_dateThird");
					String vacationFo = rs.getString("vacation_dateFour");
					String name = rs.getString("name");

	                vacationBeans vacationBeans = new vacationBeans(Id,userIdDate, vacationF,vacationS,vacationT,vacationFo,name);

	                vacationList.add(vacationBeans);
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        } finally {
	            // データベース切断
	            if (conn != null) {
	                try {
	                    conn.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                    return null;
	                }
	            }
	        }
	        return vacationList;
	    }

	  public List<vacationBeans> VacVac(String names) {
			Connection conn = null;
			List<vacationBeans> vacationList = new ArrayList<vacationBeans>();

			try {
				// データベースへ接続
				conn = DBmanager.getConnection();

				// SELECT文を準備
				// TODO: 未実装：管理者以外を取得するようSQLを変更する
				String sql = "SELECT vacation.id, vacation.user_id, vacation.vacation_dateFirst, vacation.vacation_dateSecond, vacation.vacation_dateThird, vacation.vacation_dateFour, user.name FROM vacation INNER JOIN user user ON vacation.user_id = user.id where user.name <> '河野菜摘'";


				if(names.length() > 0) {
					sql += " and name LIKE '%" + names + "%'";
				}


				System.out.println(sql);
				// SELECTを実行し、結果表を取得
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(sql);


				// 結果表に格納されたレコードの内容を
				// Userインスタンスに設定し、ArrayListインスタンスに追加
				while (rs.next()) {
					 int Id = rs.getInt("id");
						String userIdDate = rs.getString("user_id");
						String vacationFi = rs.getString("vacation_dateFirst");
						String vacationSe = rs.getString("vacation_dateSecond");
						String vacationT = rs.getString("vacation_dateThird");
						String vacationFo = rs.getString("vacation_dateFour");
						String nameDate = rs.getString("name");

					vacationBeans vacationBeans = new vacationBeans(Id,userIdDate, vacationFi,vacationSe,vacationT,vacationFo,nameDate);

					vacationList.add(vacationBeans);
				}
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			} finally {
				// データベース切断
				if (conn != null) {
					try {
						conn.close();
					} catch (SQLException e) {
						e.printStackTrace();
						return null;
					}
				}
			}
			return vacationList;
		}



	 public void vacationUp(String userId, String vacationDateFirst,String vacationDateSecond,String vacationDateThird,String vacationDateFour) {

			//No value specified for parameter 4　とはsql文の？？？？が４つなのに対して
			//pStmt.setStringの数が４つないときに出る。数をそろえてあげる。

			Connection conn = null;

			try {

				conn = DBmanager.getConnection();

				String sql = "INSERT INTO vacation (user_id,vacation_dateFirst,vacation_dateSecond,vacation_dateThird,vacation_dateFour) VALUES (?,?,?,?,?) ON DUPLICATE KEY UPDATE vacation_dateFirst =?,vacation_dateSecond =?,vacation_dateThird =?,vacation_dateFour =?";

				PreparedStatement pStmt = conn.prepareStatement(sql);


				pStmt.setString(1,userId);
				pStmt.setString(2,vacationDateFirst);
				pStmt.setString(3,vacationDateSecond);
				pStmt.setString(4,vacationDateThird);
				pStmt.setString(5,vacationDateFour);
				pStmt.setString(6,vacationDateFirst);
				pStmt.setString(7,vacationDateSecond);
				pStmt.setString(8,vacationDateThird);
				pStmt.setString(9,vacationDateFour);



				pStmt.executeUpdate();

			} catch (SQLException e) {

				e.printStackTrace();

			} finally {

				if (conn != null) {

					try {

						conn.close();

					} catch (SQLException e) {

						e.printStackTrace();

					}

				}

			}

		}

	 public vacationBeans vacLast(String userId, String vacationDateFirst) {
			Connection conn = null;

			try {
				// データベース接続
				conn = DBmanager.getConnection();
				// 実行SQL文字列定義
				String sql = " SELECT * FROM vacation WHERE DATE_FORMAT(vacation_dateFirst, '%Y%m') = DATE_FORMAT(NOW(), '%Y%m') AND user_id = ?;";

				PreparedStatement pStmt = conn.prepareStatement(sql);
				// SQLの?パラメータに値を設定
				pStmt.setString(1,userId);

				ResultSet rs = pStmt.executeQuery();
				 if (!rs.next()) {
		                return null;
		            }

				    String uId = rs.getString("user_id");
					String vacationF = rs.getString("vacation_dateFirst");
		            return new vacationBeans(uId,vacationF);
		        } catch (SQLException e) {
		            e.printStackTrace();
		            return null;
		        } finally {
		            // データベース切断
		            // 以下findAllと同じ処理なので略
		        }
		    }



}

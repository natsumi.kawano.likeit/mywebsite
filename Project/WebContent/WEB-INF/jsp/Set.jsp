<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<title>完了</title>
    <!-- BootstrapのCSS読み込み -->
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- オリジナルCSS読み込み -->
    <link href="css/original/common.css" rel="stylesheet">
</head>
<body>
<form action="HopeServlet" method="post">
   <nav class="navbar navbar-expand-lg navbar-light" style="background-color:#EDFFBE;">
  <a class="navbar-brand" href="UserListServlet">休み希望日を送信しました</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav mr-auto">
    </ul>
    <span class="navbar-text">
        </span>
  </div>
</nav>
<div class="HOPE">
        <div class="container">
           <div class="form-group">
			<img src="img/doraemon.png" alt="いせかい" width="250">
               <font size="3">送信が完了しました</font>
               </div>
            <a class="btn btn-primary" href="HopeServlet?id=${userInfo.id}">お休み希望</a>
            <a class="btn btn-success" href ="AllServlet">お休み希望表一覧</a>
            <a class="btn btn-dark" href="UserUpServlet?id=${userInfo.id}">ユーザ情報更新</a>
            <a class="btn btn-warning" href="UserListServlet">　　ユーザ一覧へ　　</a>
            <a class="btn btn-info" href="LogoutServlet">　　ログアウト　　</a>
</div>
</div>
</form>
</body>
</html>
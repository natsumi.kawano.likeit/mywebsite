<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<title>休み希望表</title>
    <!-- BootstrapのCSS読み込み -->
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- オリジナルCSS読み込み -->
    <link href="css/original/common.css" rel="stylesheet">
</head>
<body>
<form action="AllServlet" method="post">
<!-- header -->
<nav class="navbar navbar-expand-lg navbar-light" style="background-color:#EDFFBE;">
<a class="navbar-brand" href="UserListServlet">みんなの休み希望表</a>
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
<span class="navbar-toggler-icon"></span>
</button>
<div class="collapse navbar-collapse" id="navbarText">
<ul class="navbar-nav mr-auto">
</ul>
<span class="navbar-text">
<c:if test="${userInfo.usertype == 0}">
<a href="UserNewServlet">ユーザ新規登録</a>
</c:if>
　　　<a href="LogoutServlet">ログアウト</a>
</span>
</div>
</nav>
	<div class="container">
	<div class="s-area">
		<h1 class="userSearch">
		<img src="img/daradara.png" alt="だらだら"  width="120">nAmeSearch
		<img src="img/gorogoro.png" alt="ごろごろ" width="200"></h1>
    <div class="form-group row">
    	<label for="text" class="col-sm-2 col-form-label">naME</label>
    <div class="col-sm-10">
    	<input type="text" class="form-control" name="nameS" placeholder="naME">
    </div>
    </div>
    </div>
    <div class="SEARCH row">
	<!-- Indicates caution should be taken with this action -->
		<button type="submit" class="btn btn-warning btn-sm">searCH</button>
	</div>
	</div>
    	<table class="table">
      	<thead style="background-color:#EDFFBE;">
        <tr>
          <th scope="col">nAme</th>
          <th scope="col">vacation</th>
          <th scope="col"><img src="img/yubiha-to.png" width="80"></th>
          <th scope="col"><img src="img/ha-to.png" width="50"></th>
          <th scope="col"><img src="img/ganbarou.png" width="50"></th>
        </tr>
        </thead>
        <tbody>
      	<c:forEach var="vacation" items="${vacationList}">
        	<tr>
          		<td>${vacation.username}さん</td>
          		<td>${vacation.vacationDateFirst}</td>
            	<td> ${vacation.vacationDateSecond}</td>
             	<td>${vacation.vacationDateThird}</td>
             	<td>${vacation.vacationDateFour}</td>
     		</tr>
     	</c:forEach>
      	</tbody>
    	</table>
</form>
</body>
</html>
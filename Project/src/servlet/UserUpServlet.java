package servlet;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.userDao;
import model.userBeans;

/**
 * Servlet implementation class UserUpServlet
 */
@WebServlet("/UserUpServlet")
public class UserUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		HttpSession session = request.getSession();
		if(session.getAttribute("userInfo") == null) {
			response.sendRedirect("LoginServlet");
			return;
			}

		String id = request.getParameter("id");
		userDao userdao = new userDao();
		userBeans user = userdao.empUp(Integer.parseInt(id));



		request.setAttribute("id", id);
		request.setAttribute("user", user);




		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/UserUp.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");


		// リクエストパラメータの入力項目を取得
		String id = request.getParameter("id");
		String loginId = request.getParameter("loginId");
		String passwordF = request.getParameter("passwordF");
		String passwordS = request.getParameter("passwordS");
		String name = request.getParameter("name");
		String birthDate= request.getParameter("birthDate");
		userDao dao = new userDao();

		 try {
				dao.Md5(passwordF);
			} catch (NoSuchAlgorithmException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}

		 boolean isErro = false;

		 if(!(passwordF.equals(passwordS))){

			 isErro = true;
		 }

		 if(name.length() == 0 || birthDate.length() == 0){

			 isErro = true;
		 }

		 if(isErro) {
			 request.setAttribute("errMsg", "入力された内容は正しくありません");


				userBeans user2 = new userBeans();
				user2.setId(Integer.parseInt(id));
				user2.setLoginId(loginId);
				user2.setPassword(passwordF);
				user2.setPassword(passwordS);
				user2.setName(name);
				user2.setBirthDate(birthDate);
				request.setAttribute("user", user2);

				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserUp.jsp");
				dispatcher.forward(request, response);
			return;
		 }


		dao.UpDate(Integer.parseInt(id),passwordF,name,birthDate);
		dao.newUserUp(Integer.parseInt(id), name, birthDate);


		response.sendRedirect("UserListServlet");
	}

}

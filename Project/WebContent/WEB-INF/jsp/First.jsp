<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<title>休み希望日提出</title>
    <!-- BootstrapのCSS読み込み -->
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- オリジナルCSS読み込み -->
    <link href="css/original/common.css" rel="stylesheet">
</head>
<body>
	<form action="FirstServlet" method="post">
	<input type="hidden" name="id" value="${send.id}">
	<!-- header -->
	<nav class="navbar navbar-expand-lg navbar-light" style="background-color:#EDFFBE;">
  		<a class="navbar-brand" href="UserListServlet">休み希望日提出ツール
  		</a>
	</nav>
	<div class="container">
	<div class="INFO">
		<a href="LoginServlet"><img src="img/tap.png" alt="tap">
		</a>
	</div>
	</div>
	</form>
</body>
</html>
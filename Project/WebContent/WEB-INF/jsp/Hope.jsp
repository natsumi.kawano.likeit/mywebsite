<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<title>休み希望日</title>
    <!-- BootstrapのCSS読み込み -->
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- オリジナルCSS読み込み -->
    <link href="css/original/common.css" rel="stylesheet">
</head>
<body>
	<form action="HopeServlet" method="post">
	<input type="hidden" name="userId" value="${user.id}">
	<nav class="navbar navbar-expand-lg navbar-light" style="background-color:#EDFFBE;">
  		<a class="navbar-brand" href="UserListServlet">${user.name}さんの休み希望日
  		</a>
  		 <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    		<span class="navbar-toggler-icon">
    		</span>
  	 	 </button>
  	<div class="collapse navbar-collapse" id="navbarText">
     <ul class="navbar-nav mr-auto">
     </ul>
     <span class="navbar-text">
      <a href="LogoutServlet">ログアウト
      </a>
     </span>
  	</div>
	</nav>
	<div class="container">
    <div class="form-group">
    <div class="HOPE">
	 <img src="img/yarukinashi.png" alt="やるきなし" width="150">
     	<font size="5">休み希望日
     	</font>
	 <input type="text" name="userId" class="form-control mx-sm-3" value="${user.loginId}" readonly>
	 <input type="date" name="vacationDateFirst" class="form-control mx-sm-3">
	 <input type="date" name="vacationDateSecond" class="form-control mx-sm-3">
	 <input type="date" name="vacationDateThird" class="form-control mx-sm-3">
	 <input type="date" name="vacationDateFour" class="form-control mx-sm-3">
      <small id="vacationdate" class="text-muted">
      </small>
    	<font color="red">※ここから登録してください
    	</font><br>
     <div class="INSERT">
	   <button type="submit" class="btn btn-warning btn-sm">　　提出　　
	   </button>
	 </div>
	 </div>
     <table class="table">
      <thead style="background-color:#EDFFBE;">
       <tr>
        <th scope="col">loGIniD
        </th>
        <th scope="col">nAme
        </th>
        <th scope="col">biRThdATe
        </th>
        <th scope="col">
        </th>
        </tr>
      </thead>
     <tbody>
      <tr>
       <th scope="row">${user.loginId}
       </th>
          <td>${user.name}
          </td>
          <td>${user.birthDate}
          </td>
          <td>
          <a class="btn btn-warning" href="AllServlet">お休み希望表
          </a>
           <a class="btn btn-secondary" href="UserUpServlet?id=${user.id }">更新
           </a>
           <a class="btn btn-primary" href ="UserInfoServlet?id=${user.id }">詳細
           </a>
            <c:if test="${userInfo.usertype == 0}">
            <a class="btn btn-dark" href="DeleteServlet?id=${user.id}">削除
            </a>
            </c:if>
           </td>
        </tr>
       </tbody>
       </table>
       </div>
    <a href ="UserListServlet">戻る
    </a>
    </div>
    </form>
</body>
</html>
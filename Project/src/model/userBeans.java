package model;
import java.io.Serializable;
import java.text.SimpleDateFormat;

public class userBeans implements Serializable{
	private int id;
	private String loginId;
	private String name;
	private String birthDate;
	private String password;
	private String createDate;
	private String updateDate;
	private String usertype;


	// ログインセッションを保存するためのコンストラクタ
		public userBeans(String loginId, String name){
			this.loginId = loginId;
			this.name = name;
		}

		public userBeans(int id, String loginId, String name, String birthDate, String password, String createDate,
				String updateDate,String usertype) {
			this.id = id;
			this.loginId = loginId;
			this.name = name;
			this.birthDate = birthDate;
			this.password = password;
			this.createDate = createDate;
			this.updateDate = updateDate;
			this.usertype = usertype;
			}

		public userBeans(int id, String loginId, String name, String birthDate,String createDate,
				String updateDate,String usertype) {
			this.id = id;
			this.loginId = loginId;
			this.name = name;
			this.birthDate = birthDate;
			this.createDate = createDate;
			this.updateDate = updateDate;
			this.usertype = usertype;
			}

		public userBeans(String loginId) {
		this.loginId = loginId;
		}





	public userBeans() {
		this.id = id;
		this.loginId = loginId;
		this.name = name;
		this.birthDate = birthDate;
		this.password = password;
		this.createDate = createDate;
		this.updateDate = updateDate;
		this.usertype = usertype;
		}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

	public String getUsertype() {
		return usertype;
	}

	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}

	public String getFormatDatec() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日HH時mm分");
		return sdf.format(createDate);
	}

	public String getFormatDateu() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy年MM月dd日HH時mm分");
		return sdf.format(updateDate);
	}


}

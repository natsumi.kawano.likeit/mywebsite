package model;

import java.io.Serializable;

public class vacationBeans implements Serializable{

	private int id;
	private String userId;
	private String vacationDateFirst;
	private String vacationDateSecond;
	private String vacationDateThird;
	private String vacationDateFour;
	private String username;





	public vacationBeans(int id, String userId, String vacationDateFirst) {
		this.id = id;
		this.userId = userId;
		this.vacationDateFirst = vacationDateFirst;
	}

	public vacationBeans(String userId, String vacationDateFirst) {
		this.userId = userId;
		this.vacationDateFirst = vacationDateFirst;
	}

	public vacationBeans(int id, String userId, String vacationDateFirst, String vacationDateSecond, String vacationDateThird,
			String vacationDateFour) {
		this.id = id;
		this.userId = userId;
		this.vacationDateFirst = vacationDateFirst;
		this.vacationDateSecond = vacationDateSecond;
		this.vacationDateThird = vacationDateThird;
		this.vacationDateFour = vacationDateFour;
}

	public vacationBeans(int id, String userId, String vacationDateFirst, String vacationDateSecond, String vacationDateThird,
			String vacationDateFour,String username) {
		this.id = id;
		this.userId = userId;
		this.vacationDateFirst = vacationDateFirst;
		this.vacationDateSecond = vacationDateSecond;
		this.vacationDateThird = vacationDateThird;
		this.vacationDateFour = vacationDateFour;
		this.username = username;


	}

	public void vacationBeans() {

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getVacationDateFirst() {
		return vacationDateFirst;
	}

	public void setVacationDateFirst(String vacationDateFirst) {
		this.vacationDateFirst = vacationDateFirst;
	}

	public String getVacationDateSecond() {
		return vacationDateSecond;
	}

	public void setVacationDateSecond(String vacationDateSecond) {
		this.vacationDateSecond = vacationDateSecond;
	}

	public String getVacationDateThird() {
		return vacationDateThird;
	}

	public void setVacationDateThird(String vacationDateThird) {
		this.vacationDateThird = vacationDateThird;
	}

	public String getVacationDateFour() {
		return vacationDateFour;
	}

	public void setVacationDateFour(String vacationDateFour) {
		this.vacationDateFour = vacationDateFour;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}











}

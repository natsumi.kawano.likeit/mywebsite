<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ユーザ削除</title>
    <!-- BootstrapのCSS読み込み -->
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- オリジナルCSS読み込み -->
    <link href="css/original/common.css" rel="stylesheet">
</head>
<body>
<form action="DeleteServlet" method="post">
	<input type="hidden" name="id" value="${user.id}">
<!-- header -->
<nav class="navbar navbar-expand-lg navbar-light" style="background-color:#EDFFBE;">
  	<a class="navbar-brand" href="UserListServlet">ユーザ情報削除</a>
  	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  	</button>
  	<div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav mr-auto">
    </ul>
    <span class="navbar-text">
    <c:if test="${userInfo.usertype == 0}">
     <a href="UserNewServlet">ユーザ新規登録</a></c:if>　　　
    <a href="LogoutServlet">ログアウト</a>
    </span>
  	</div>
</nav>
<div class="container">
<div class="userSearch">
	<img src="img/dokan.png" alt="どかん" width="200">${user.loginId}さんのデータを本当に削除してよろしいでしょうか？<br>
	<div class="form-group row">
    <div class="DELETE" >
<!-- Indicates caution should be taken with this action -->
	</div>
    </div>
     <button type="submit" class="btn btn-warning btn-sm">　　　dELeTE　　　
	 </button>
</div>
<a href ="http://localhost:8080/MyWeebSite/UserListServlet">戻る
	</a>
</div>
</form>
</body>
</html>
package dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.SendBeans;

public class SendDao {

	  public List<SendBeans> findAll() {
	        Connection conn = null;
	        List<SendBeans> sendList = new ArrayList<SendBeans>();

	        try {
	            // データベースへ接続
	            conn = DBmanager.getConnection();

	            // SELECT文を準備
	            // TODO: 未実装：管理者以外を取得するようSQLを変更する
	            String sql = "SELECT * FROM send";

	             // SELECTを実行し、結果表を取得
	            Statement stmt = conn.createStatement();
	            ResultSet rs = stmt.executeQuery(sql);

	            // 結果表に格納されたレコードの内容を
	            // Userインスタンスに設定し、ArrayListインスタンスに追加
	            while (rs.next()) {
	                int id = rs.getInt("id");
	                String nameId = rs.getString("name_id");
	                String title = rs.getString("title");
	                String sendMail = rs.getString("send_mail");
	                String sendDate = rs.getString("send_datetime");
	                SendBeans sendbeans = new SendBeans(id, nameId, title,sendMail, sendDate);

	                sendList.add(sendbeans);
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        } finally {
	            // データベース切断
	            if (conn != null) {
	                try {
	                    conn.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                    return null;
	                }
	            }
	        }
	        return sendList;
	    }

	  public void sendIns(String nameId, String title, String sendMail) {
			Connection conn = null;

			try {
				// データベース接続
				conn = DBmanager.getConnection();
				// 実行SQL文字列定義
				String sql = "INSERT INTO send(name_id, title, send_mail, send_datetime) VALUES(?,?,?,now())";

				PreparedStatement pStmt = conn.prepareStatement(sql);

				// SQLの?パラメータに値を設定
				pStmt.setString(1, nameId);
				pStmt.setString(2, title);
				pStmt.setString(3, sendMail);

				// 登録SQL実行
				pStmt.executeUpdate();

			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				try {
					// コネクションインスタンスがnullでない場合、クローズ処理を実行
					if (conn != null) {
						conn.close();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}


 public void sendDel(int id) {
	Connection conn = null;

	try {
		// データベース接続
		conn = DBmanager.getConnection();
		// 実行SQL文字列定義
		String sql = "DELETE FROM send WHERE id = ?";

		PreparedStatement pStmt = conn.prepareStatement(sql);

		// SQLの?パラメータに値を設定
		pStmt.setInt(1, id);


		// 登録SQL実行
		pStmt.executeUpdate();

	} catch (SQLException e) {
		e.printStackTrace();
	} finally {
		try {
			// コネクションインスタンスがnullでない場合、クローズ処理を実行
			if (conn != null) {
				conn.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}

 public SendBeans searchUp(int Id) {

		Connection conn = null;

		try {

			conn = DBmanager.getConnection();

			String sql = "SELECT id, name_id, title, send_mail, send_datetime FROM send WHERE id =" + Id;

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			if (!rs.next()) {

				return null;
			}

			int id = rs.getInt("id");
			String nameId = rs.getString("name_id");
			String title = rs.getString("title");
			String sendmail = rs.getString("send_mail");
			String senddatetime = rs.getString("send_datetime");

			return new SendBeans(id,nameId,title,sendmail,senddatetime);

		} catch (SQLException e) {

			e.printStackTrace();

			return null;

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

	}

}


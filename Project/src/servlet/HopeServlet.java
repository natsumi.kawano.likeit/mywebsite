package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.userDao;
import dao.vacationDao;
import model.userBeans;
import model.vacationBeans;
/**
 * Servlet implementation class HopeServlet
 */
@WebServlet("/HopeServlet")
public class HopeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public HopeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//doGet  URLの名前にそろえる。http://localhost:8080/MyWeebSite/HopeServlet?id=1 このidに
		//oPost  jspの${loginId}の中の名前や<input name="AAA"> のAAAにそろえる。 注意！！
		HttpSession session = request.getSession();
		if(session.getAttribute("userInfo") == null) {
			response.sendRedirect("LoginServlet");
			return;
			}

		String id = request.getParameter("id");
		userDao userdao = new userDao();
		userBeans user = userdao.empUp(Integer.parseInt(id));

		request.setAttribute("user", user);


		vacationDao vac = new vacationDao();
		vacationBeans vb = vac.vacationJoin(id);

		request.setAttribute("vb", vb);

		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/Hope.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");


		// リクエストパラメータの入力項目を取得

		String userId = request.getParameter("userId");
		String vacationF = request.getParameter("vacationDateFirst");
		String vacationS = request.getParameter("vacationDateSecond");
		String vacationT = request.getParameter("vacationDateThird");
		String vacationFo = request.getParameter("vacationDateFour");

		vacationDao vacdao = new vacationDao();



		vacdao.vacationUp(userId,replaceNull(vacationF),replaceNull(vacationS),replaceNull(vacationT),replaceNull(vacationFo));
		response.sendRedirect("SetServlet");

	}

	String replaceNull(String value) {
		if(value.length() == 0) {
			return null;
		}
		return value;

	}

}

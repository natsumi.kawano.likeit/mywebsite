<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ユーザ情報更新</title>
<!-- BootstrapのCSS読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!-- オリジナルCSS読み込み -->
<link href="css/original/common.css" rel="stylesheet">
</head>
<body>
	<form action="UserUpServlet" method="post">
	<input type="hidden" name="id" value="${user.id}">
		<nav class="navbar navbar-expand-lg navbar-light" style="background-color:#EDFFBE;">
			<a class="navbar-brand" href="UserListServlet">登録情報変更</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarText" aria-controls="navbarText"
				aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarText">
				<ul class="navbar-nav mr-auto">
				</ul>
				<span class="navbar-text">
				<c:if test="${userInfo.usertype == 0}">
     <a href="UserNewServlet">ユーザ新規登録</a></c:if>　　　
				<a href="LogoutServlet">ログアウト</a>
				</span>
			</div>
		</nav>

		<div class="container">
			<div class="s-area">
				<h1 class="userSearch"><img src="img/uchu.png" alt="うちゅう" height="80">
				ユーザ情報更新<img src="img/hako.png" alt="はこ" height="150"></h1>
				<div class="form-group row">
					<label for="inputloginId" class="col-sm-2 col-form-label">loGIniD</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="loginId" value="${user.loginId}"
							readonly>
							</div>
							</div>
							<div class="form-group row">
					<label for="inputPassword" class="col-sm-2 col-form-label">PaSswORd</label>
					<div class="col-sm-10">
						<input type="password" class="form-control" name="passwordF" id="PasswordFrom"
							placeholder="PasSwORd">
							</div>
							</div>
				<div class="form-group row">
					<label for="inputPassword" class="col-sm-2 col-form-label">PaSswORd(rEPeat)</label>
					<div class="col-sm-10">
						<input type="password" class="form-control" name="passwordS" id="PasswordFor"
							placeholder="PasSwORd">
					</div>
				</div>
				<div class="form-group row">
					<label for="inputname" class="col-sm-2 col-form-label">naME</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" name="name" id="PasswordFor"
							value="${user.name}">
					</div>
				</div>

				<div class="form-group">
					<label for="inputbirthDate">biRThdATe</label> <input type="date"
						id="birthDateFrom" name="birthDate" class="form-control mx-sm-3" value="${user.birthDate}">
						<small
						id="birthdate" class="text-muted"> </small> <input type="hidden"
						name="updateDate" value="<%=System.currentTimeMillis()%>">
				</div>
<span ><font color="red">${errMsg}</font></span><br>

				<div class="INSERTUP">

					<!-- Indicates caution should be taken with this action -->
					<button type="submit" class="btn btn-warning btn-sm form-submit">
						updaTE</button>

				</div>
			</div>

			<a href="http://localhost:8080/MyWeebSite/UserListServlet">戻る</a>
		</div>

	</form>
</body>
</html>
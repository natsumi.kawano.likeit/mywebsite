<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ユーザ情報</title>
    <!-- BootstrapのCSS読み込み -->
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- オリジナルCSS読み込み -->
    <link href="css/original/common.css" rel="stylesheet">
</head>
<body>
<form action="UserInfoServlet" method="post">
<input type="hidden" name="id" value="${id}">
<!-- header -->
<nav class="navbar navbar-expand-lg navbar-light" style="background-color:#EDFFBE;">
  <a class="navbar-brand" href="UserListServlet">ユーザ情報</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav mr-auto">
    </ul>
    <span class="navbar-text">
    <c:if test="${userInfo.usertype == 0}">
     <a href="UserNewServlet">ユーザ新規登録</a></c:if>　　　
     <a href="LogoutServlet">ログアウト</a>
    </span>
  </div>
</nav>
<div class="container">
<img src="img/UFO.png" width="300" alt="UFO">
<div class="INFO">
ログインID : ${loginId}<br>
なまえ : ${name}<br>
生年月日 : ${birthDate}<br>
登録日時 : ${createDate}<br>
更新日時 : ${updateDate}<br>
<a href ="http://localhost:8080/MyWeebSite/UserListServlet">戻る</a>
    </div>
    </div>
</form>
</body>
</html>
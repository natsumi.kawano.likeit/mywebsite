package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.userBeans;

public class userDao {

	 /**
     * ログインIDとパスワードに紐づくユーザ情報を返す
     * @param loginId
     * @param password
     * @return
     */
    public userBeans findByLoginInfo(String loginId, String password) {
        Connection conn = null;
        try {
            // データベースへ接続
            conn = DBmanager.getConnection();

            // SELECT文を準備
            String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

             // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, loginId);
            pStmt.setString(2, password);
            ResultSet rs = pStmt.executeQuery();


            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
            if (!rs.next()) {
                return null;
            }

            // 必要なデータのみインスタンスのフィールドに追加
            int id = rs.getInt("id");
            String loginIdDate = rs.getString("login_id");
            String name = rs.getString("name");
            String birthDate = rs.getString("birth_date");
            String passwordDate = rs.getString("password");
            String createDate = rs.getString("create_date");
            String updateDate = rs.getString("update_date");
            String usertype = rs.getString("user_type");
            return new userBeans(id, loginIdDate, name, birthDate, passwordDate, createDate, updateDate,usertype);

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }

    /**
     * 全てのユーザ情報を取得する
     * @return
     */
    public List<userBeans> findAll() {
        Connection conn = null;
        List<userBeans> userList = new ArrayList<userBeans>();

        try {
            // データベースへ接続
            conn = DBmanager.getConnection();

            // SELECT文を準備
            // TODO: 未実装：管理者以外を取得するようSQLを変更する
            String sql = "SELECT * FROM user where login_id <> 'admin'";

             // SELECTを実行し、結果表を取得
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            // 結果表に格納されたレコードの内容を
            // Userインスタンスに設定し、ArrayListインスタンスに追加
            while (rs.next()) {
                int id = rs.getInt("id");
                String loginId = rs.getString("login_id");
                String name = rs.getString("name");
                String birthDate = rs.getString("birth_date");
                String password = rs.getString("password");
                String createDate = rs.getString("create_date");
                String updateDate = rs.getString("update_date");
                String usertype = rs.getString("user_type");
                userBeans userbeans = new userBeans(id, loginId, name, birthDate, password, createDate, updateDate,usertype);

                userList.add(userbeans);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return userList;
    }

    public List<userBeans> findSearch(String loginIdP,String nsme,String birthDateP,String birthDateS) {
		Connection conn = null;
		List<userBeans> userList = new ArrayList<userBeans>();

		try {
			// データベースへ接続
			conn = DBmanager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM user where login_id <> 'admin'";

			if(loginIdP.length() > 0) {
				sql += " and login_id = '" + loginIdP + "'";
			}

			if(nsme.length() > 0) {
				sql += " and name LIKE '%" + nsme + "%'";
			}

			if(!birthDateP.equals("")) {
				sql += " and birth_date >= '" + birthDateP + "'";
			}

			if(!birthDateS.equals("")) {
				sql += " and birth_date <= '" + birthDateS + "'";
			}


			System.out.println(sql);
			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);


			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				 int id = rs.getInt("id");
	                String loginId = rs.getString("login_id");
	                String name = rs.getString("name");
	                String birthDate = rs.getString("birth_date");
	                String password = rs.getString("password");
	                String createDate = rs.getString("create_date");
	                String updateDate = rs.getString("update_date");
	                String usertype = rs.getString("user_type");
				userBeans user = new userBeans(id, loginId, name, birthDate, password, createDate, updateDate,usertype);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

    public userBeans empUp(int Id) {

		Connection conn = null;

		try {

			conn = DBmanager.getConnection();

			String sql = "SELECT id, login_id, name, birth_date, create_date, update_date, user_type FROM user WHERE id =" + Id;

			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			if (!rs.next()) {

				return null;
			}

			int id = rs.getInt("id");
			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			String birthDate = rs.getString("birth_date");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");
			String usertypeDate = rs.getString("user_type");

			return new userBeans(id,loginId,name, birthDate, createDate, updateDate,usertypeDate);

		} catch (SQLException e) {

			e.printStackTrace();

			return null;

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}

	}

    public userBeans newUse(String loginId) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBmanager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);

			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String loginIdDate = rs.getString("login_id");

			return new userBeans(loginIdDate);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}



    public void userInsert(String loginId, String password, String name, String birthDate) {
		Connection conn = null;

		try {
			// データベース接続
			conn = DBmanager.getConnection();
			// 実行SQL文字列定義
			String sql = "INSERT INTO user(login_id, password, name, birth_date, create_date, update_date, user_type) VALUES(?,?,?,?,now(),now(),1)";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			// SQLの?パラメータに値を設定
			pStmt.setString(1, loginId);
			pStmt.setString(2, password);
			pStmt.setString(3, name);
			pStmt.setString(4, birthDate);

			// 登録SQL実行
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (conn != null) {
					conn.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

    public void Delete(int id) {

		Connection conn = null;

		try {

			conn = DBmanager.getConnection();

			String sql = "DELETE FROM user WHERE id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			// SQLの?パラメータに値を設定
			pStmt.setInt(1,id);

			pStmt.executeUpdate();

		} catch (SQLException e) {

			e.printStackTrace();

		} finally {

			if (conn != null) {

				try {

					conn.close();

				} catch (SQLException e) {

					e.printStackTrace();

				}

			}

		}

	}

    public void UpDate(int id,String password, String name, String birthDate) {

		//No value specified for parameter 4　とはsql文の？？？？が４つなのに対して
		//pStmt.setStringの数が４つないときに出る。数をそろえてあげる。

		Connection conn = null;

		try {

			conn = DBmanager.getConnection();

			String sql = "UPDATE user SET password =?, name =?, birth_date = ?, update_date = now(), user_type = 1 WHERE id =?";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1,password);
			pStmt.setString(2, name);
			pStmt.setString(3,birthDate);
			pStmt.setInt(4,id);


			pStmt.executeUpdate();

		} catch (SQLException e) {

			e.printStackTrace();

		} finally {

			if (conn != null) {

				try {

					conn.close();

				} catch (SQLException e) {

					e.printStackTrace();

				}

			}

		}

	}

    public void newUserUp(int id, String name, String birthDate) {

		//No value specified for parameter 4　とはsql文の？？？？が４つなのに対して
		//pStmt.setStringの数が４つないときに出る。数をそろえてあげる。

		Connection conn = null;

		try {

			conn = DBmanager.getConnection();

			String sql = "UPDATE user SET name = ?, birth_date = ?, update_date = now() WHERE id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);

			pStmt.setString(1, name);
			pStmt.setString(2,birthDate);
			pStmt.setInt(3,id);


			pStmt.executeUpdate();

		} catch (SQLException e) {

			e.printStackTrace();

		} finally {

			if (conn != null) {

				try {

					conn.close();

				} catch (SQLException e) {

					e.printStackTrace();

				}

			}

		}

	}

    public String Md5(String password) throws NoSuchAlgorithmException{

		//ハッシュを生成したい元の文字列
		String source = password;
		//ハッシュ生成前にバイト配列に置き換える際のCharset
		Charset charset = StandardCharsets.UTF_8;
		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
		String result = DatatypeConverter.printHexBinary(bytes);
		//標準出力
		System.out.println(result);

		return result;
}


}













<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>新規登録</title>
<!-- BootstrapのCSS読み込み -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!-- オリジナルCSS読み込み -->
<link href="css/original/common.css" rel="stylesheet">
</head>
<body>
	<form action="UserNewServlet" method="post">
		<input type="hidden" name="id" value="${id }">
		<!-- header -->
		<nav class="navbar navbar-expand-lg navbar-light" style="background-color:#EDFFBE;">
			<a class="navbar-brand">ユーザ新規登録</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarText" aria-controls="navbarText"
				aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarText">
				<ul class="navbar-nav mr-auto">
				</ul>
				<span class="navbar-text"> <a
					href="http://localhost:8080/MyWeebSite/LoginServlet">ログアウト</a>
				</span>
			</div>
		</nav>
		<div class="container">
		<div class="s-area">
				<h1 class="userSearch"><img src="img/iine.png" width="150">新規登録</h1>
				<div class="form-group row">
					<label for="inputloginId" class="col-sm-2 col-form-label">loGIniD</label>
					<div class="col-sm-10">
						<input type="text" name="loginId" class="form-control"
							id="loginId" placeholder="absde123" value="${user.loginId}">
					</div>
				</div>
				<div class="form-group row">
					<label for="inputPassword" class="col-sm-2 col-form-label">PaSswORd</label>
					<div class="col-sm-10">
						<input type="password" name="passwordF" class="form-control"
							id="PasswordF" placeholder="PasSwORd" >
					</div>
				</div>
				<div class="form-group row">
					<label for="inputPassword" class="col-sm-2 col-form-label">PaSswORd(rEPeat)</label>
					<div class="col-sm-10">
						<input type="password" name="passwordS" class="form-control"
							id="PasswordS" placeholder="PasSwORd">
					</div>
				</div>
				<div class="form-group row">
					<label for="text" class="col-sm-2 col-form-label">nAMe</label>
					<div class="col-sm-10">
						<input type="text" name="name" class="form-control"
							id="name" placeholder="nAMe" value="${user.name}">
					</div>
				</div>
					<div class="form-group">
					<label for="inputbirthDate">biRThdATe</label> <input type="date"
						name="birthDate" id="birthDateFrom" class="form-control mx-sm-3" value="${user.birthDate}">
					<small id="birthdate" class="text-muted"> </small> <input
						type="hidden" name="createDate"
						value="<%=System.currentTimeMillis()%>"> <input
						type="hidden" name="updateDate"
						value="<%=System.currentTimeMillis()%>">
						<input
						type="hidden" name="vacationF"
						value="0000-00-00"> <input
						type="hidden" name="vacationS"
						value="0000-00-00">
						<input
						type="hidden" name="vacationT"
						value="0000-00-00"> <input
						type="hidden" name="vacationFo"
						value="0000-00-00">
						</div>
						<span ><font color="red">${errMsg}</font></span><br>
				<div class="INSERTUP">
<!-- Indicates caution should be taken with this action -->
					<button type="submit" class="btn btn-warning btn-sm form-submit" value="${id }">
						iNSeRt</button>
						</div>
						</div>
<a href="http://localhost:8080/MyWeebSite/UserListServlet">戻る</a>
		</div>
	</form>
</body>
</html>
package model;

import java.io.Serializable;

public class SendBeans implements Serializable{

	private int id;
	private String nameId;
	private String title;
	private String sendMail;
	private String sendDate;


	public SendBeans(int id,String nameId, String title, String sendMail, String sendDate) {
			this.id = id;
			this.nameId = nameId;
			this.title = title;
			this.sendMail = sendMail;
			this.sendDate = sendDate;
		}



	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNameId() {
		return nameId;
	}
	public void setNameId(String nameId) {
		this.nameId = nameId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getSendMail() {
		return sendMail;
	}
	public void setSendMail(String sendMail) {
		this.sendMail = sendMail;
	}
	public String getSendDate() {
		return sendDate;
	}
	public void setSendDate(String sendDate) {
		this.sendDate = sendDate;
	}





}

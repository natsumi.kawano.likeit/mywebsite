package servlet;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.userDao;
import model.userBeans;



/**
 * Servlet implementation class UserNewServlet
 */
@WebServlet("/UserNewServlet")
public class UserNewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserNewServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		if(session.getAttribute("userInfo") == null) {
			response.sendRedirect("LoginServlet");
			return;
			}

		RequestDispatcher dispatcher=request.getRequestDispatcher("/WEB-INF/jsp/UserNew.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("loginId");
		String passwordF = request.getParameter("passwordF");
		String passwordS = request.getParameter("passwordS");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");

		userDao userdao = new userDao();
		userBeans user = userdao.newUse(loginId);

		try {
			userdao.Md5(passwordF);
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}

		//パスワードが同じ値
		//全てのフォームに入力されてるのとき登録可能

		boolean isError = false;

		if (user != null) {
			isError = true;
		}

		if (!(passwordF.equals(passwordS))) {
			isError = true;
		}

		//フォームの未入力にはエラーを出す

		if (loginId.length() == 0 || passwordF.length() == 0 || passwordS.length() == 0 || name.length() == 0
				|| birthDate.length() == 0) {
			isError = true;
		}

		if(isError) {
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			userBeans user2 = new userBeans();
			user2.setLoginId(loginId);
			user2.setPassword(passwordF);
			user2.setPassword(passwordS);
			user2.setName(name);
			user2.setBirthDate(birthDate);


			request.setAttribute("user", user2);


			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/UserNew.jsp");
			dispatcher.forward(request, response);
			return;
		}

		userdao.userInsert(loginId, passwordF, name, birthDate);
		//vacationDao vacation = new vacationDao();
		//vacation.vacationInsert(loginId,vacationF, vacationS, vacationT, vacationFo);
		response.sendRedirect("UserListServlet");


	}

}

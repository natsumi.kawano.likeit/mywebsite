<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<title>共有掲示板</title>
    <!-- BootstrapのCSS読み込み -->
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- オリジナルCSS読み込み -->
    <link href="css/original/common.css" rel="stylesheet">
</head>
<body>
<form action="SendServlet" method="post">
<!-- header -->
<nav class="navbar navbar-expand-lg navbar-light" style="background-color:#EDFFBE;">
  <a class="navbar-brand" href="UserListServlet">みんなの共有掲示板
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon">
    </span>
  </button>
  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav mr-auto">
    </ul>
    <span class="navbar-text">
    <c:if test="${userInfo.usertype == 0}">
     <a href="UserNewServlet">ユーザ新規登録
     </a>
    </c:if>
     　　　<a href="LogoutServlet">ログアウト
           </a>
    </span>
  </div>
</nav>
<div class="container">
<div class="s-area">
	<h1 class="userSearch">
    	<img src="img/denkyu.png" width="40" alt="denkyu">meSSAgE
        <img src="img/denkyu.png" width="40" alt="denkyu">
	</h1>
</div>
 NaMe<input type="text" name="nameId" class="form-control" value="${userInfo.name}" readonly>
 tITlE<input type="text" name="title" class="form-control" placeholder="Text input">
 MeSSAgE<textarea class="form-control" name="sendMail" rows="3" placeholder="MeSSagEを入力してください"></textarea>
  <input type="hidden" name="sendDate" value="<%=System.currentTimeMillis()%>">
</div>
<div class="SEARCH row">
	<!-- Indicates caution should be taken with this action -->
	<a href="mailto:natsumi.kawano.likeit@gmail.com"><img src="img/message.png" width="150" alt="管理者宛メール">
	</a>
    <button type="submit" class="btn btn-warning btn-sm">SEnD
    </button>
     <img src="img/JK.png" alt="女子高生" width="150">
</div>
    <table class="table">
    	<thead style="background-color:#EDFFBE;">
         <tr>
          <th scope="col">NaMe
          </th>
          <th scope="col">tITlE
          </th>
          <th scope="col">
          </th>
          <th scope="col">dATe
          </th>
           <th scope="col">
           </th>
          </tr>
         </thead>
         <tbody>
      <c:forEach var="send" items="${sendList}">
        <tr>
          <td>${send.nameId}
          </td>
          <td>${send.title}
          </td>
            <td> ${send.sendMail}
            </td>
             <td>${send.sendDate}
             </td>
             <td>
              <a class="btn btn-warning" href="ResDlvServlet?id=${send.id}">削除
              </a>
             </td>
     	</tr>
       </c:forEach>
       </tbody>
    </table>
</form>
</body>
</html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ユーザ一覧</title>
    <!-- BootstrapのCSS読み込み -->
   <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- オリジナルCSS読み込み -->
    <link href="css/original/common.css" rel="stylesheet">
</head>
<body>
<form action="UserListServlet" method="post">
<input type="hidden" name="sendId" value="${send.id}">
<!-- header -->
<nav class="navbar navbar-expand-lg navbar-light" style="background-color:#EDFFBE;">
  <a class="navbar-brand" href="UserListServlet">登録者一覧</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav mr-auto">
</ul>
    <span class="navbar-text">
    <c:if test="${userInfo.usertype == 0}">
     <a href="UserNewServlet">ユーザ新規登録</a></c:if>　　　<a href="LogoutServlet">ログアウト</a>
    </span>
  </div>
</nav>
<div class="container">
<div class="s-area">
          <h1 class="userSearch">
          <img src="img/denkyu.png" width="40">uSerSearch
          <img src="img/denkyu.png" width="40"></h1>
          <div class="form-group row">
        <label for="text" class="col-sm-2 col-form-label">loGIniD</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" name="loginS" placeholder="absde123">
        </div>
      </div>
      <div class="form-group row">
        <label for="text" class="col-sm-2 col-form-label">naME</label>
        <div class="col-sm-10">
          <input type="text" class="form-control" name="nameS" placeholder="naME">
        </div>
          </div>
           <div class="form-group">
        <label for="inputbirthDate">biRThdATe</label>
    　　　　　　　<input type="date" name="birthDateFS" class="form-control mx-sm-3">　fRoM
               　　<input type="date" name="birthDateTS" class="form-control mx-sm-3">
    　　　　　<small id="birthdate" class="text-muted">
    </small>
  </div>
</div>
<div class="SEARCH row">
	<!-- Indicates caution should be taken with this action -->
	<button type="submit" class="btn btn-warning btn-sm">searCH</button>
</div>
<a class="btn btn-danger" href="AllServlet">お休み希望表一覧</a>
             <a class="btn btn-info" href="SendServlet">みんなの共有掲示板</a>
    <table class="table">
      <thead style="background-color:#EDFFBE;">
        <tr>
          <th scope="col">loGIniD</th>
          <th scope="col">nAme</th>
          <th scope="col">biRThdATe</th>
          <th scope="col"></th>
        </tr>
      </thead>
      <tbody>
      <c:forEach var="user" items="${userList}">
        <tr>
          <td>${user.loginId}</td>
          <td>${user.name}</td>
          <td>${user.birthDate}</td>
			<td>
          	<c:if test="${userInfo.usertype == 0 or userInfo.loginId == user.loginId}">
            <a class="btn btn-warning" href="HopeServlet?id=${user.id}">お休み希望</a></c:if>
			 <c:if test="${userInfo.usertype == 0 or userInfo.loginId == user.loginId}">
            <a class="btn btn-info" href="UserUpServlet?id=${user.id}">更新</a></c:if>
			<c:if test="${userInfo.usertype == 0 or userInfo.loginId == user.loginId}">
            <a class="btn btn-primary" href ="UserInfoServlet?id=${user.id}">詳細</a></c:if>
			<c:if test="${userInfo.usertype == 0}">
            <a class="btn btn-dark" href="DeleteServlet?id=${user.id}">削除</a></c:if>
            </td>
     			</tr>
     			    </c:forEach>
     			    </tbody>
     			    </table>
     			    </div>
     			    </form>
</body>
</html>